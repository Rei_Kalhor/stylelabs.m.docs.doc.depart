
(function () {

  importScripts('lunr.min.js');

  var lunrIndex;
  var currentVersion = getUrlVar("v");
  var stopWords = null;
  var searchData = {};

  //\s matches a single whitespace
  //\- matches a single dash
  //\. matches a single dot
  //the seprator of the lun is any number of space, dash or dots or any combination of them
  lunr.tokenizer.seperator = /[\s\-\.]+/;

  var stopWordsRequest = new XMLHttpRequest();
  //it gets any common english words like is-am-are-can-...
  stopWordsRequest.open('GET', '../search-stopwords.json');
  stopWordsRequest.onload = function () {
    if (this.status != 200) {
      return;
    }
    stopWords = JSON.parse(this.responseText);
    buildIndex();
  }
  stopWordsRequest.send();

  var searchDataRequest = new XMLHttpRequest();

  searchDataRequest.open('GET', '../index.json');
  searchDataRequest.onload = function () {
    if (this.status != 200) {
      return;
    }
    searchData = JSON.parse(this.responseText);

    buildIndex();

    postMessage({ e: 'index-ready' });
  }
  searchDataRequest.send();

  onmessage = function (oEvent) {
    var q = oEvent.data.q;
    console.log(oEvent.data);
    var hits = lunrIndex.search(q);
    var results = [];
    hits.forEach(function (hit) {
      var item = searchData[hit.ref];
      results.push({ 'href': item.href, 'title': item.title, 'keywords': item.keywords });
    });
    postMessage({ e: 'query-ready', q: q, d: results });
  }



  function urlMatchWithVersion(url, version) {
    if (version == null) return false;
    var version_regex = /\d+\.\d+.x/;
    var match = url.match(version_regex);
    if (match != null) return match[0] === version;
    return false;
  }

  function getUrlVars() {
    var vars = {};
    self.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi,
      function (m, key, value) {
        vars[key] = value;
      });
    return vars;
  }

  function getUrlVar(getParam) {
    return getUrlVars()[getParam];
  }

  function buildIndex() {
    if (stopWords !== null && !isEmpty(searchData)) {
      lunrIndex = lunr(function () {
        this.pipeline.remove(lunr.stopWordFilter);
        this.ref('href');
        this.field('title', { boost: 50 });
        this.field('keywords', { boost: 20 });

        for (var prop in searchData) {

          if (urlMatchWithVersion(prop, currentVersion) && searchData.hasOwnProperty(prop)) {
            this.add(searchData[prop]);
          }
        }

        var docfxStopWordFilter = lunr.generateStopWordFilter(stopWords);
        lunr.Pipeline.registerFunction(docfxStopWordFilter, 'docfxStopWordFilter');
        this.pipeline.add(docfxStopWordFilter);
        this.searchPipeline.add(docfxStopWordFilter);
      });
    }
  }

  function isEmpty(obj) {
    if (!obj) return true;

    for (var prop in obj) {
      if (obj.hasOwnProperty(prop))
        return false;
    }

    return true;
  }
})();
