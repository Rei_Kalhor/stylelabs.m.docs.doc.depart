(function cstmGA() {

	// Check if it's not on a development environment
	if( !location.hostname.match('stylelabsdev.com') ){

		// UA key storage
		var keys = {
			"external" : "UA-113158386-1", // docs.stylelabs.com
			"internal" : "UA-113158386-2" //docs-internal.stylelabs.com
		}

		// Check if it's internal or external
		var keyPlaceholder = location.hostname.match('docs-internal') ? keys.internal : keys.external;

		// Load GTAG script
		var GTAGscriptUrl = "https://www.googletagmanager.com/gtag/js?id=" + keyPlaceholder
		
		var script = document.createElement('script');
		script.type = 'text/javascript';
		script.async = true;
		script.src = GTAGscriptUrl;

		document.head.appendChild(script);

		// When script has been loaded
		script.onload = function() {
			// GTAG script execution
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());
			gtag('config', keyPlaceholder);
		}
	}
})()
