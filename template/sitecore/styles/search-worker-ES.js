(function () {

    console.log("I am in search worker ES");
    var searchData = {};

    //it should be send
    var searchDataRequest = new XMLHttpRequest();
    searchDataRequest.open('GET', '../index.json');
    searchDataRequest.onload = function () {
        if (this.status != 200) {
            return;
        }
        searchData = JSON.parse(this.responseText);

        postMessage({
            e: 'index-ready-ES'
        });
    }
    searchDataRequest.send();

    onmessage = function (oEvent) {
        var q = oEvent.data.q;
        
        //it should be replace by the result of search 
        var results = [];
        var hits = q;
        //content/2.10.x/integrations/advanced/intro.html
        let ref = "";

        console.log("in search-worker-es-----------------",  hits);
        hits.forEach(function (hit) {
            ref = "content/" + hit._source["file_version"] + "/" + hit._source["main_category"] + "/" 
            + hit._source["directory"] +  "/" + hit._source["file_name"].replace("md", "html");
            console.log("ref" , ref);
            var item = searchData[ref];
            console.log("item" , item);
            if(item) results.push({ 'href': item.href, 'title': item.title, 'keywords': item.keywords });
        });

        //it should be constructed based on content gotten from elastic seacg
        postMessage({
            e: 'query-ready-ES',
            q: q,
            d: results
        });
    }

    function isEmpty(obj) {
        if (!obj) return true;

        for (var prop in obj) {
            if (obj.hasOwnProperty(prop))
                return false;
        }

        return true;
    }

})();